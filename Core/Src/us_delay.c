/*
 * make sure that HSE_VALUE is set
 */

#include "us_delay.h"

static uint32_t	Clk_Cyc_Per_uS;

void us_DELAY_init()
{
	SystemCoreClockUpdate();                                // Update SystemCoreClock value
	Clk_Cyc_Per_uS = SystemCoreClock / 1000000;

	if (!(CoreDebug->DEMCR & CoreDebug_DEMCR_TRCENA_Msk))   // DWT_Init
	{
		CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
		DWT->CYCCNT = 0;
		DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
	}
}

void us_DELAY(uint32_t us)
{
	int32_t tp = DWT->CYCCNT + us * Clk_Cyc_Per_uS;
	while (((int32_t) DWT->CYCCNT - tp) < 0);
}

