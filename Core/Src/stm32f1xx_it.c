
#include "main.h"
#include "stm32f1xx_it.h"

volatile uint32_t reference;
volatile uint32_t raw_freq;
volatile uint8_t measurement_ready;
volatile uint16_t timeout;
volatile uint16_t wait_time;
volatile uint8_t show_wait_time;
volatile uint8_t gate_time;
volatile uint32_t idle_time;
volatile uint8_t clear_data;


void NMI_Handler(void)
{
	while (1)
	{

	}
}


void HardFault_Handler(void)
{
	while (1)
	{

	}
}


void MemManage_Handler(void)
{
	while (1)
	{

	}
}


void BusFault_Handler(void)
{
	while (1)
	{

	}
}


void UsageFault_Handler(void)
{
	while (1)
	{

	}
}


void SVC_Handler(void)
{

}


void DebugMon_Handler(void)
{

}


void PendSV_Handler(void)
{

}


void SysTick_Handler(void)
{
	static uint8_t teller;

	static uint8_t pin_b11;									//debouncing PB11, PB12 and PB13
	static uint8_t pin_b12;									//all connected to the rotary encoder
	static uint8_t pin_b13;

	pin_b11 <<= 1;
	pin_b12 <<= 1;
	pin_b13 <<= 1;

	if (LL_GPIO_IsInputPinSet(GPIOB, LL_GPIO_PIN_11)) pin_b11++;
	if (LL_GPIO_IsInputPinSet(GPIOB, LL_GPIO_PIN_12)) pin_b12++;
	if (LL_GPIO_IsInputPinSet(GPIOB, LL_GPIO_PIN_13)) pin_b13++;

	pin_b11 &= 0x0F;										//only use nibbles, so that the pin only has
	pin_b12 &= 0x0F;										//to be high/low for 4 ms instead of 8 ms or
	pin_b13 &= 0x0F;										//else we miss pulses from the encoder

	if (pin_b11 == 0x08)									//rotation detected
	{
		clear_data = 1;
		timeout = 10;										//restart end current measurement after 10 ms
		idle_time = 20;

		if ((pin_b12 == 0x00) && (gate_time < 59)) gate_time++;
		if ((pin_b12 == 0x0F) && (gate_time > 0)) gate_time--;
		if (gate_time > 59) gate_time = 9;					//just in case... reset to gate_time = 1 second
	}

	if (pin_b13 == 0x08)									//button press detected
	{
		clear_data = 1;
	}




	if (teller < 4) teller++;
	else
	{
		teller = 0;											//produce a 100 Hz signal
		LL_GPIO_TogglePin(GPIOA, LL_GPIO_PIN_15);			//for tuning the OCXO
	}



	if (timeout)											//defines the approx gate-time
	{
		timeout--;

		wait_time = ((timeout - 1) / 1000) + 1;				//calc wait time so you know how long to
		if ((timeout % 1000) == 0) show_wait_time = 1;		//wait for the next measurement result
	}
	else
	{
		LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_11);		//set D-flipflop HIGH
		show_wait_time = 1;
	}



	if (idle_time) idle_time--;								//for the check for input measurement_ready
}



void TIM1_CC_IRQHandler(void)
{
	static uint32_t prev_raw_freq;							//all CH1 channels of TIM1, TIM2, TIM3 and TIM4 are
	static uint32_t prev_reference;							//triggered at the same time, the values are combined 
	uint32_t new_raw_freq;									//to the raw_freq and reference
	uint32_t new_reference;


	if (LL_TIM_IsActiveFlag_CC1(TIM1))
	{
		LL_TIM_ClearFlag_CC1(TIM1);

		new_raw_freq = (LL_TIM_IC_GetCaptureCH1(TIM2) << 16) | LL_TIM_IC_GetCaptureCH1(TIM1);
		new_reference = (LL_TIM_IC_GetCaptureCH1(TIM4) << 16) | LL_TIM_IC_GetCaptureCH1(TIM3);

		raw_freq = new_raw_freq - prev_raw_freq;
		reference = new_reference - prev_reference;

		prev_raw_freq = new_raw_freq;
		prev_reference = new_reference;

		measurement_ready = 1;
	}
}


void TIM2_IRQHandler(void)									//interrupts not enabled
{
	if (LL_TIM_IsActiveFlag_CC1(TIM2))
	{
		LL_TIM_ClearFlag_CC1(TIM2);
	}

}


void TIM3_IRQHandler(void)									//interrupts not enabled
{
	if (LL_TIM_IsActiveFlag_CC1(TIM3))
	{
		LL_TIM_ClearFlag_CC1(TIM3);
	}
}


void TIM4_IRQHandler(void)									//interrupts not enabled
{
	if (LL_TIM_IsActiveFlag_CC1(TIM4))
	{
		LL_TIM_ClearFlag_CC1(TIM4);
	}

}


//void USART1_IRQHandler(void)								//see USART1.C
//{

//}


